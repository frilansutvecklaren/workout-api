const express = require('express');
const router = express.Router();
const trainingDay = require('./trainingday.service');

// Routes
router.get('/', getAll);
router.get('/:id', getById);
router.post('/add', create);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function create(req, res, next) {
  trainingDay.create(req.body)
    .then(() => res.json())
    .catch(err => next(err));
}

function getAll(req, res, next) {
  trainingDay.getAll()
    .then(users => res.json(users))
    .catch(err => next(err));
}

function getById(req, res, next) {
  trainingDay.getById(req.params.id)
    .then(user => user ? res.json(user) : res.sendStatus(404))
    .catch(err => next(err));
}

function update(req, res, next) {
  trainingDay.update(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function _delete(req, res, next) {
  trainingDay.delete(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}