const db = require('../_helpers/db');
const TrainingDay = db.TrainingDay;

module.exports = {
  getAll,
  getById,
  create,
  update,
  delete: _delete
};

async function getAll() {
  return await TrainingDay.find().select();
}

async function getById(id) {
  return await TrainingDay.findById(id).select();
}

async function create(trainingDayParam) {
  const trainingDay = new TrainingDay(trainingDayParam);
  await trainingDay.save();
}

async function update(id, trainingDayParam) {
  const trainingDay = await TrainingDay.findById(id);

  if (!trainingDay) throw 'Training day not found';

  Object.assign(trainingDay, trainingDayParam);

  await trainingDay.save();
}

async function _delete(id) {
  await TrainingDay.findByIdAndRemove(id);
}
