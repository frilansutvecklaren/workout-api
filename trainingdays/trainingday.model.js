const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const exerciseSets = new Schema({
  repetitions: { type: Number, default: 0 },
  value: { type: Number, default: 0 }
});

const exercise = new Schema({
  name: { type: String },
  unit: { type: String },
  sets: [exerciseSets],
});

const trainingDay = new Schema({
  userId: { type: String, required: true },
  exercises: [exercise],
  trainingDayDate: { type: Date, default: Date.now },
  createdDate: { type: Date, default: Date.now }
});

trainingDay.set('toJSON', { virtuals: true });

module.exports = mongoose.model('TrainingDay', trainingDay);
