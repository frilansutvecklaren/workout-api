require('rootpath')();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('./_helpers/jwt');
const compression = require('compression');
const helmet = require('helmet');
const errorHandler = require('./_helpers/error-handler');

const app = express();

app.use(compression());
app.use(helmet());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(jwt());

app.use('/users', require('users/users.controller'));
app.use('/exercises', require('exercises/exercises.controller'));
app.use('/trainingdays', require('trainingdays/trainingdays.controller'));

app.use(errorHandler);

module.exports = app;
