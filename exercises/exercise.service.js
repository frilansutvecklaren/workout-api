const db = require('../_helpers/db');
const Exercise = db.Exercise;

module.exports = {
  getAll,
  getById,
  create,
  update,
  delete: _delete
};

async function getAll() {
  return await Exercise.find();
}

async function getById(id) {
  return await Exercise.findById(id);
}

async function create(exerciseParam) {
  if (await Exercise.findOne({ exercise: exerciseParam.exercise })) {
    throw 'Exercise "' + exerciseParam.exercise + '" already exists';
  }

  const exercise = new Exercise(exerciseParam);

  await exercise.save();
}

async function update(id, exerciseParam) {
  const exercise = await Exercise.findById(id);

  if (!exercise) throw 'Exercise not found';

  if (exercise.exercise !== exerciseParam.exercise && await Exercise.findOne({ exercise: exerciseParam.exercise })) {
    throw 'Exercise "' + exerciseParam.ëxercise + '" already exists';
  }

  Object.assign(exercise, exerciseParam);

  await exercise.save();
}

async function _delete(id) {
  await Exercise.findByIdAndRemove(id);
}