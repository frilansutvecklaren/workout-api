const express = require('express');
const router = express.Router();
const exerciseService = require('./exercise.service');

// Routes
router.post('/add', create);
router.get('/', getAll);
router.get('/:id', getById);
router.put('/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function getAll(req, res, next) {
  exerciseService.getAll()
    .then(exercises => res.json(exercises))
    .catch(err => next(err));
}

function getById(req, res, next) {
  exerciseService.getById(req.params.id)
    .then(exercise => exercise ? res.json(exercise) : res.sendStatus(404))
    .catch(err => next(err));
}

function create(req, res, next) {
  exerciseService.create(req.body)
    .then(() => res.json())
    .catch(err => next(err));
}

function update(req, res, next) {
  exerciseService.update(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err));
}

function _delete(req, res, next) {
  exerciseService.delete(req.params.id)
    .then(() => res.json({}))
    .catch(err => next(err));
}