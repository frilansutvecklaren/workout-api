const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const exercise = new Schema({
  name: { type: String, required: true },
  unit: { type: String },
  description: { type: String },
  createdDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Exercise', exercise);